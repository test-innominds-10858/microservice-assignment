package com.innominds.microservice.currencyconversionservice;

import java.math.BigDecimal;
import java.math.MathContext;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyConversionController {

	@GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionBean convertCurrency(@PathVariable String from, @PathVariable String to,
			@PathVariable BigDecimal quantity) {
		
		MathContext mc = new MathContext(4); // 4 precision
		
		BigDecimal exRate= new BigDecimal(76.64);
		BigDecimal calculatedAmound = exRate.multiply(quantity,mc);
		return new CurrencyConversionBean(1001L,from,to,exRate,quantity,calculatedAmound,8001);

	}

}
